#!/usr/bin/env bash
# backup 
#source : https://docs.gitlab.com/ee/raketasks/backup_restore.html

d=$(date +%Y-%m-%d)
mkdir "$d""_gitlab-backup" && cd "$d""_gitlab-backup" || exit

# backup Rake Task 
sudo gitlab-backup create GZIP_RSYNCABLE=yes

# Storing configurations files
sudo cp /etc/gitlab/gitlab-secrets.json .
sudo cp /etc/gitlab/gitlab.rb .
