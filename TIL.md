# Postgres + WSL2
01/05/2022
  - [WSL 2 Static IP](https://stevegy.medium.com/wsl-2-static-ip-341603d84401)
  - [How to connect to windows postgres Database from WSL](https://stackoverflow.com/a/67596486)
  - [Enable remote access to Postgres](https://www.cyberciti.biz/tips/postgres-allow-remote-access-tcp-connection.html)
  - [How to Allow Remote Connection to PostgreSQL Database using psql](https://www.thegeekstuff.com/2014/02/enable-remote-postgresql-connection/)


# Gitlab upgrade + backup
25-04-2022
  - https://docs.gitlab.com/ee/raketasks/backup_restore.html
  - script [gitlab-backup.sh](./gitlab-backup.sh)

# Fail2ban
25-04-2022
  - https://www.linuxtechi.com/install-use-fail2ban-on-rhel-8-centos-8/
  - https://www.cyberciti.biz/faq/how-to-protect-ssh-with-fail2ban-on-centos-8/
  - https://computerz.solutions/ssh-config-fail2ban-selinux/

# Salt + Podman(Docker)
21-04-2022
  - [saltstack/salt](https://hub.docker.com/r/saltstack/salt)
  - [Podman & Buildah for docker users](https://developers.redhat.com/blog/2019/02/21/podman-and-buildah-for-docker-users#)
  - 

# Gitlab + Kubernetes + Google Cloud : Agent Setup
18-04-2022
  - [GitLab Kubernetes Agent Setup Walkthrough - Round 2 (youtube)](https://www.youtube.com/watch?v=XuBpKtsgGkE)
  - create repository > https://git.dar2017.dedyn.io/-/ide/project/abdel/kubernets-agent-setup-with-ui
  - Kubernetes Agent configuration https://docs.gitlab.com/ee/user/clusters/agent/install/#create-an-agent-configuration-file
  - [How to deploy the GitLab Agent for Kubernetes with limited permissions](https://about.gitlab.com/blog/2021/09/10/setting-up-the-k-agent/)

# Gitlab + Kubernetes + Google Cloud
18-04-2022  
  - GitLab and Google Cloud Kubernetes cluster integration
    - Create free Google Cloud Platform (GCP)
    - Google OAuth 2.0 OmniAuth Provider > https://git.dar2017.dedyn.io/help/integration/google
    - Manage Cluster applications > https://git.dar2017.dedyn.io/help/user/clusters/management_project_template.md


# Gitlab/Github 
18-04-2022
  - Gitlab/Github integration via oauth apps > https://docs.github.com/en/developers/apps/building-oauth-apps/creating-an-oauth-app

# Gitlab installation
16-04-2022  
  - Install GitLab on RHEL8 > https://about.gitlab.com/install/?version=ce#centos-7
  - Install Postfix and test with mailx > https://linuxconfig.org/how-to-install-postfix-on-redhat-8
  - Desec.io + Let's Encrypt > OK

# python (anaconda) + Visual Studio Code GitLab integration
13-04-2022
  - install anaconda on Ubuntu@WSL2 → https://www.digitalocean.com/community/tutorials/how-to-install-anaconda-on-ubuntu-18-04-quickstart
  - Visual Studio Code GitLab integration → https://docs.gitlab.com/ee/user/project/repository/vscode.html
  
# python (Debian & RHEL)
12-04-2022
  - upgrade Python3 (3.7 to 3.9)
    - Debian : https://linuxize.com/post/how-to-install-python-3-9-on-debian-10/
    - RHEL7 : https://tecadmin.net/install-python-3-9-on-centos/
    
# ansible
12-04-2022
 - ansible-navigator > https://play.instruqt.com/embed/redhat/tracks/getting-started-ansible-navigator/challenges/ansible-navigator-explore-ui/notes

# podman on WSL2
12-04-2022
https://www.redhat.com/sysadmin/podman-windows-wsl2

# WSL2 + Docker
10-04-2022
  - convert distro to WSL2
  - Docker Desktop integration

# Containers
10-04-2022
  - kubernetes-getting-started (online course) → https://www.udemy.com/course/kubernetes-getting-started
  - Nomad (HashiCorp) → https://www.nomadproject.io/ → https://learn.hashicorp.com/tutorials/nomad/get-started-intro?in=nomad/get-started
    - https://learn.hashicorp.com/tutorials/nomad/nomad-pack-intro?in=nomad/nomad-pack
      - Install Go → https://go.dev/doc/install


# GitLab + Let's Encrypt + Desec.io
29-03-2022
  - update cert


# Saltstack, Rundeck + MySQL
24-03-2022
  - install Rundeck via Salt State

# ServiceNow
24-03-2022
  - CMDB, CI, ITOM, ITAM, HAM, SAM, ... : definitions

# Linux
24-03-2022
* logrotate : remove files older than x days

# GitLab Pages + CI principles
20-03-2022
* setup GitLab Pages
  - create DNS Alias (CNAME)
  - create TXT Record for ownership verification
  - Automatic Let's Encrypt Certificate
[doc](https://docs.gitlab.com/ee/user/project/pages/)

* install ruby environnement and Jekyll
  - [install rbenv via git checkout](https://github.com/rbenv/rbenv#basic-github-checkout)
  - [install rbbuild as a rbenv module](https://github.com/rbenv/ruby-build)
    - prereq. for Debian 11 :
      - apt install gcc make bzip2 libssl-dev zlib1g-dev
      - etc.
  - check with rbenv-doctor: `curl -fsSL https://github.com/rbenv/rbenv-installer/raw/main/bin/rbenv-doctor | bash`

*  Deploy site with GitLab CI/CD
  - [Deploy site](https://docs.gitlab.com/ee/user/project/pages/getting_started/pages_from_scratch.html#deploy-and-view-your-website)
    - Pushes with continuous tests to feature branches.
    - Caches dependencies installed with Bundler.
    - Continuously deploys every push to the main branch.

# Linux
19-03-2022
* visudo editor (add to profile)
```bash
export SUDO_EDITOR=vim
export EDITOR=vim
```

# WSL
19-03-2022
* install new fresh distro (clone)

# git
19-03-2022
* create repository and upload to gitlab.com
https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html


# GitLab

19-03-2022

* install jekyll from template


* Create a GitLab Pages website from scratch
[Tutorial: Create a GitLab Pages website from scratch](https://docs.gitlab.com/ee/user/project/pages/getting_started/pages_from_scratch.html)

* Install GitLab Runner on Linux
 - [doc](https://docs.gitlab.com/runner/install/linux-manually.html)
 - [via official repository (deb, rpm)](https://docs.gitlab.com/runner/install/linux-repository.html)
 - [via direct download(deb, rpm)](https://docs.gitlab.com/runner/install/linux-manually.html#using-debrpm-package)



# docker
